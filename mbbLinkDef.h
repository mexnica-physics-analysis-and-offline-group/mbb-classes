// $Id: MbbLinkDef.h, v 1.0 2019/06/25 18:43:00  Exp $

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MbbDetector+;
#pragma link C++ class MbbPoint+;
#pragma link C++ class MbbGeo+;
#pragma link C++ class MbbGeoPar+;


#endif
